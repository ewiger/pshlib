from pshlib.psh import psh


def test_basic_sh():
    res = psh(
    'VAR=world',
    """
        echo This is
            a multiline hello
            $VAR!
    """,
    ).output
    print(res)
    excepted = 'This is a multiline hello world!\n'
    assert excepted == res